import React, { Component } from 'react'
import { Table, Button, Container,Image } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import axios from 'axios'
import Pagination from "react-js-pagination";
import swal from 'sweetalert';
export default class Search extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: {
                DATA: []
            },
            perPage: 5,
            activePage: 1
        }
    };
    componentWillMount(){
        axios.get(`http://110.74.194.124:15011/v1/api/articles?page=1&limit=20`).then(res => {
            this.setState({data: res.data});
        });
    }
    convertDate=(dateStr)=>{
        var dateString = dateStr;
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);
        var day = dateString.substring(6, 8);
        var date = year + "-" + month + "-" + day;
        return date;
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({activePage: pageNumber});
    }
    handleDelete=(id)=>{
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(res=>{
                    this.componentWillMount()
                })
              swal("Data has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Data is safe!");
            }
          });
        // axios.delete(`http://110.74.194.124:15011/v1/api/articles/${id}`).then(res=>{
        //     this.componentWillMount()
        // })
    }
    render() {
        const resultSearch = this.props.match.params.title
        const article = this.state.data.DATA.filter((a)=>{
            return a.TITLE.toLowerCase().indexOf(this.props.match.params.title.toLowerCase())!==-1
        })
        const indexOfLast =this.state.activePage * this.state.perPage;
        const indexOfFirst = indexOfLast - this.state.perPage;
        const current = article.slice(indexOfFirst, indexOfLast);
        const myData = current.map((d)=>
            <tbody key={d.ID}>
                <tr>
                    <td>{d.ID}</td>
                    <td style={{width: '20%'}}>{d.TITLE}</td>
                    <td style={{width: '25%'}}>{d.DESCRIPTION}</td>
                    <td style={{width: '10%'}}>{this.convertDate(d.CREATED_DATE)}</td>
                    <td style={{width: '15%'}}>
                        <Image src={d.IMAGE} thumbnail />
                    </td>
                    <td style={{width: '40%'}}>
                        <Link to={`/view/${d.ID}`}>
                            <Button style={{marginRight: 3}} variant="info">VIEW</Button>
                        </Link>
                        <Link to={`/edit/${d.ID}`}>
                            <Button style={{marginRight: 3}} variant="warning">EDIT</Button>
                        </Link>
                        <Button variant="danger" onClick={(id)=> this.handleDelete(d.ID)}>DELETE</Button>
                    </td>
                </tr>
            </tbody>
        )
        return (
            <Container>
                <h1>Article Management</h1>
                <h3 style={{textAlign: "start",background: "yellow"}}>Search Results: {resultSearch}</h3>
                <div style={{textAlign: "start"}}>
                    <Link to='/'><Button variant="dark">Back</Button></Link>
                </div>
                <Table striped bordered hover style={{marginTop: 50}}>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>TITLE</th>
                            <th>DESCRIPTION</th>
                            <th>CREATED_DATE</th>
                            <th>IMAGE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                        {myData}
                </Table>
                <div style={{marginLeft: '35%'}}>
                <Pagination
                    prevPageText='prev'
                    nextPageText='next'
                    firstPageText='first'
                    lastPageText='last'
                    activePage={this.state.activePage}
                    itemsCountPerPage={this.state.perPage}
                    totalItemsCount={article.length}
                    itemClass="page-item"
                    linkClass="page-link"
                    onChange={this.handlePageChange.bind(this)}
                 />
                 </div>
            </Container>
        )
    }
}
