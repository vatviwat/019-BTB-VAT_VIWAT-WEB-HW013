import React, { Component } from 'react'
import { Container, Form, Button, Row, Col,Image} from 'react-bootstrap'
import axios from 'axios';
import swal from 'sweetalert';

export default class Add extends Component {
    constructor(){
        super();
        this.state = {
            TITLE: '',
            DESCRIPTION: '',
            IMAGE: 'https://www.renovabike.it/wp-content/themes/gecko/assets/images/placeholder.png',
            validateTitle: '',
            validateDesc: ''
        }
    }
    onInput=(e)=>{
        this.setState({[e.target.name]: e.target.value}); 
    }
    onImageChange = e => {
        if (e.target.files && e.target.files[0]) {
          this.setState({
            IMAGE: URL.createObjectURL(e.target.files[0]),
          });
        }
    };
    fileSelectedHandler=(e)=>{
        this.refs.add.click();
    }
    handleValidation(){
        let fields = this.state;
        let isValid = true;
        if(!fields["TITLE"]){
          isValid = false;
          this.setState({validateTitle: "* Title cannot be empty"})
        }
        if(!fields["DESCRIPTION"]){
          isValid = false;
          this.setState({validateDesc: "* Description cannot be empty"})
        }
        return isValid;
    }    
    onSubmit=(e)=>{
        e.preventDefault();
        if(this.handleValidation()){
            const {TITLE,DESCRIPTION,IMAGE}=this.state
            console.log(TITLE);
            console.log(DESCRIPTION);
            console.log(IMAGE)
            axios.post(`http://110.74.194.124:15011/v1/api/articles`,{
                TITLE,
                DESCRIPTION,
                IMAGE,
            })
            .then(res =>{
                swal("Successfully!", "Data has been added", "success");
                window.history.back()
            })
            .catch(err=>
                console.log(err)
            )
        }
    }
    render() {
        return (
            <Container style={{textAlign: 'start'}}>
                <h1 style={{marginTop: '2%',background: 'black',color: 'white',textAlign: 'center'}}>Add Article</h1>
                <Row>
                    <Col lg="6">
                        <Form id="myform" style={{marginTop: '10%'}}>
                            <Form.Group>
                            <Form.Label>TITLE <span style={{ fontSize: 16, color: "red" }}>{this.state.validateTitle}</span></Form.Label>
                                <Form.Control type="text" placeholder="Title" name="TITLE" value={this.state.value} onChange={this.onInput}/>
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>DESCRIPTION <span style={{ fontSize: 16, color: "red" }}>{this.state.validateDesc}</span></Form.Label>
                                <Form.Control type="text" placeholder="Description" name="DESCRIPTION" value={this.state.value} onChange={this.onInput}/>
                            </Form.Group>
                            <Button variant="dark" value="submit" onClick={this.onSubmit}>Submit</Button>
                        </Form>
                    </Col>
                    <Col lg="6">
                        <div style={{marginTop: '2%'}}>
                            <input style={{display:"none"}} ref="add" type="file" onChange={this.onImageChange}/>
                            <Image style={{marginLeft: '9%'}} id="img" onClick={this.fileSelectedHandler} src={this.state.IMAGE} alt="Responsive image" thumbnail/>
                        </div>
                    </Col>
                </Row>
            </Container>
        )
    }
}
